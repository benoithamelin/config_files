
iab `a &agrave
iab `A &Agrave
iab ^a &acirc
iab ^A &Acirc
iab 'e &eacute
iab 'E &Eacute
iab `e &egrave
iab `E &Egrave
iab ^e &ecirc
iab ^E &Ecirc
iab �e &eumlaut
iab �E &euml
iab ^i &icirc
iab ^I &Icirc
iab ^o &ocirc
iab ^O &Ocirc
iab `u &ugrave
iab `U &Ugrave
iab ^u &ucirc
iab ^U &Ucirc
iab ,c &ccedil
iab ,C &Ccedil

imap <C-j>c <code>
imap <C-k>c </code>
imap <C-j>b <strong>
imap <C-k>b </strong>
imap <C-j>i <em>
imap <C-k>i </em>
imap <C-j>u <u>
imap <C-k>u </u>
imap <C-j>s <sub>
imap <C-k>s </sub>
imap <C-j>x <sup>
imap <C-k>x </sup>

