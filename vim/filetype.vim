
if exists("did_load_filetypes")
	finish
endif

augroup filetypedetect
  au! BufNewFile,BufRead *.sce      setf scilab
  au! BufRead,BufNewFile *.rhtml    setf eruby
  au! BufRead,BufNewFile *.texttape setf texttape
augroup END

