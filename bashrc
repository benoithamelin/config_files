#!/bin/bash

if tty -s ; then
  stty -ixon
fi

# User specific aliases and functions

umask u=rwx,g=rx,o=

function prompt_git_status()
{
    X=$?
    PARSE_GIT_STATUS_AWK='\
BEGIN{ branch = "??"; nb_changed = 0; nb_unknown = 0; }
$1 == "##" {
    if(match($2, "[^.]+"))
    {
        branch = substr($2, RSTART, RLENGTH);
        if(branch ~ "master")
        {
            branch = ("\033[01;37m" branch "\033[00;37m");
        }
    }
    if(length($3) > 0 && match($4, "[0-9]+"))
    {
        if($3 ~ "\\[ahead")
        {
            color = "\033[00;32m";
            sign = "+";
        }
        else
        {
            color = "\033[00;31m";
            sign = "-";
        }
        branch = (branch color " [" sign substr($4, RSTART, RLENGTH) "]\033[0m");
    }
}
$1 ~ "[A-Z0-9]" { nb_changed++; }
$1 ~ "\\?\\?" { nb_unknown++; }
END{
    if(branch !~ "\\?\\?")
    {
        printf("\nBranch %s", branch);
        if(nb_changed > 0)
        {
            printf(", %s%d changed%s", "\033[00;32m", nb_changed, "\033[0m");
        }
        if(nb_unknown > 0)
        {
            printf(", %s%d unknown%s", "\033[00;33m", nb_unknown, "\033[0m");
        }
    }
}
'
    git version >/dev/null 2>/dev/null && \
        git status --branch --short 2>/dev/null | awk "$PARSE_GIT_STATUS_AWK"
    exit $X
}

function prompt_user_hostname()
{
    X=$?
    if [ "$USER" != "hamelin" ] || [ "$SSH_TTY" != "" ] || ls /.dockerenv >/dev/null 2>&1; then
        COLOR_HOSTNAME=""
        TEXT_MODE="02"
        COLOR="34"
        if ls /.dockerenv >/dev/null 2>&1; then
            TEXT_MODE="07"
        fi
        if [ "$USER" == "root" ]; then
            COLOR="36"
            TEXT_MODE="01"
        fi
        echo -n "[${TEXT_MODE};${COLOR}m${USER}@$(hostname -s)[0m | "
    fi
    exit $X
}

function prompt_last_exit()
{
    X=$?
    if [ $X -ne 0 ]; then
        echo -n "[01;31mLast $X[0m | "
    fi
    exit $X
}

function prompt_jobs()
{
    X=$?
    NUM_JOBS=$(jobs | grep -v 'shellmine -x' | wc -l | tr -d ' ')
    if [ $NUM_JOBS -gt 0 ]; then
        echo -n "[01;35m$NUM_JOBS job"
        if [ $NUM_JOBS -gt 1 ]; then
            echo -n "s"
        fi
        echo -n "[0m | "
    fi
    exit $X
}

function prompt_symbol()
{
    X=$?
    if [ "$UID" -eq 0 ]; then
        echo -n "[01;36m#[0m"
    else
        echo -n "\$"
    fi
    exit $?
}

export PATH=$HOME/usr/bin:$PATH
export PS1='\n$(prompt_user_hostname)$(prompt_last_exit)$(prompt_jobs)\w$(prompt_git_status)\n\A [\#] $(prompt_symbol) '
export PS2='| '
export EDITOR=vim
export RUBYPATH=$RUBYPATH:$HOME/usr/ruby
export PYTHONPATH="$PYTHONPATH:.:$HOME/usr/python"
export CLICOLOR="1"
export HOSTNAME

# Set up CLASSPATH to include stuff in the ~/usr/java directory.
export CLASSPATH=$HOME/usr/java:$CLASSPATH
#if [ -n "`ls $HOME/usr/java/*.jar`" ]; then
  for JAR in $HOME/usr/java/*.jar; do
    export CLASSPATH=$JAR:$CLASSPATH
  done
#fi
# Include local stuff.
export CLASSPATH=./classes:./src:.:$CLASSPATH

cptag() {
  COMPREPLY=( `sed -n -e "/^$2/s/^\([^\t]*\)\t.*\$/\1/p" ~/tags` )
}
complete -F cptag tag

if [ -f $HOME/.bashrc_local ]; then
    . $HOME/.bashrc_local
fi

if [ -f $HOME/.aliases ]; then
    . $HOME/.aliases
else
    echo "Hoho -- .aliases not present."
fi

export PATH="$HOME/.cargo/bin:$PATH"

if [ -z "$TMUX" ]; then
    echo "A good idea now would be to start a Tmux session, or attach to one."
else
    echo "Go forth and do great things!"
fi
