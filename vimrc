"
" Configuration de Vim (version 6.0).
"

set nocompatible
behave xterm

syntax enable
colorscheme blackboard

set encoding=utf-8

set autoindent
set backspace=eol,indent,start
set shiftwidth=4 tabstop=4
set expandtab
set cinoptions=g0,t0,+4s
set autowrite
set splitbelow splitright
set nowrap
set textwidth=78
set modeline
set modelines=2

set visualbell
set ruler
set showcmd
set hlsearch
set incsearch
set ignorecase smartcase
set showmatch
set shellslash
set relativenumber

set backup
set backupdir=$HOME/.backup,c:\\temp,c:\\tmp,~/tmp,/tmp

set wildmode=longest,list
set tags=./tags

let mapleader = "\\"

filetype on
filetype plugin indent on

let g:tex_flavor = "latex"

inoremap <C-e> <ESC>
nnoremap <NUL> :noh<CR>
inoremap <NUL> <C-o>:noh<CR>
nnoremap <C-space> <C-o>:noh<CR>
inoremap <C-space> <C-o>:noh<CR>

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

nnoremap <C-s> :wa!<CR>
nnoremap ZA :xa<CR>

nnoremap <Leader>bd :bd<CR>

let g:bufExplorerSplitOutPathName = 0
let g:bufExplorerShowRelativePath = 1

let g:pyindent_open_paren = '&sw'
let g:pyindent_continue = '&sw'

"End of script!
